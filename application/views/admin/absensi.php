<script>   
     $(function(){
     $(".cari").click(function(){
                $("#myModal").modal('show');
            })

		    $(".cari_p").click(function(){
		    var cari_p=$(this).val();
	         $.ajax({
                url:"<?php echo base_url('admin/cari_pegawai');?>",
                type:"POST",
                data:"cari_p="+cari_p,
                cache:false,
                success:function(html){
                    $("#tampilcie").html(html);
                    $("#myModal").modal('hide');
                }
            })
         })
	})

    </script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Data Absen Pegawai</h4>
                    </div>
                    <div class="modal-body">

                    <?php echo $this->session->flashdata('notif') ?>
                    
<br>

<form method="POST" action="<?php echo base_url() ?>admin/upload" enctype="multipart/form-data">
              <div class="form-group">
                <label for="exampleInputEmail1">UNGGAH FILE EXCEL</label>
                <input type="file" name="userfile" class="form-control">
              </div>

              <button type="submit" class="btn btn-success">UPLOAD</button>
            </form>


<br>

               <table id="table" class="table table-bordered table-striped">
                <thead>
                <tr>
                
              
                  <th>ID Absen</th>
                  <th>ID Pegawai</th>
                  <th>Hadir</th>
                  <th>Izin</th>
                  <th>Tidak Hadir</th>
                  <th>Bulan</th>
                  <th>Tanggal</th>
                 
                
                </tr>
                </thead>
                 <tbody>
                 <?php foreach ($data as $absen ) : ?>
                 <tr>
               
                 <td><?= $absen ['id_absen'];?></td> 
                 <td><?= $absen['id_pegawai'] ?></td> 
                 
                 <td><?= $absen['hadir'] ?></td>
                 
                 <td><?= $absen['izin'] ?></td>
                 <td><?= $absen['tidak_hadir'] ?></td>
                 <td><?= $absen['bulan'] ?></td>
                 <td><?= $absen['tanggal'] ?></td>
                 <th>
                    
                 </th>
                 </tr>
                 <?php endforeach; ?>
                 </tbody>
               </table>
               
 
                    </div>
                    <div class="modal-footer">
                       
                    </div>
                </div>
            </div>
        </div>


<table class="table table-resposive">
<tr><th>Cari Pegawai</th><td><input type="text" class="cari form-control" placeholder="Cari Pegawai ...."></td></td></tr>
</table>
<div id="tampilcie"></div>

